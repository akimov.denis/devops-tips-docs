# Kubernetes

## Troubleshooting kubernetes

[Troubleshooting and debug applications](https://kubernetes.io/docs/tasks/debug/debug-application/)

[Troubleshooting kubernetes steps](https://learnk8s.io/a/a-visual-guide-on-troubleshooting-kubernetes-deployments/troubleshooting-kubernetes.en_en.v3.pdf)

[Troubleshooting kubernetes details](https://learnk8s.io/troubleshooting-deployments)

[Determine the Reason for Pod Failure](https://kubernetes.io/docs/tasks/debug/debug-application/determine-reason-pod-failure/)

![Troubleshooting kubernetes](img/kubernetes-troubleschooting.png)

Commands:

    kubectl get pods
    kubectl describe pod <pod-name>
    kubectl get pods -o wide

    kubectl logs <pod-name>
    kubectl logs <pod-name> --previous
    kubectl describe pod <pod-name>


    kubectl port-forward <pod-name> 8080:<pod-port>
    kubectl port-forward service/<service-name> 8080:<service-port>


    kubectl describe service <service-name>
    kubectl describe ingress <ingress-name>


### Common Pods errors

Pods can have startup and runtime errors.

Startup errors include:

- ImagePullBackoff
- ImageInspectError
- ErrImagePull
- ErrImageNeverPull
- RegistryUnavailable
- InvalidImageName

Runtime errors include:

- CrashLoopBackOff
- RunContainerError
- KillContainerError
- VerifyNonRootError
- RunInitContainerError
- CreatePodSandboxError
- ConfigPodSandboxError
- KillPodSandboxError
- SetupNetworkError
- TeardownNetworkError

#### Runtime errors CreatePodSandboxError

"CreatePodSandboxError" is an error message that occurs in Kubernetes, a container orchestration platform. This error indicates that there is an issue with creating the "pod sandbox," which is an environment where containers within a pod run. A pod is a group of one or more containers that share the same network and storage resources.

To troubleshoot the CreatePodSandboxError, follow these steps:
1. Check the error message: Look for any additional information in the error message that might give you a clue about the cause of the problem. This can include issues with the container runtime, network configuration, or other related components.
2. Review pod logs: Use the command **kubectl logs <pod-name>** to check the logs of the affected pod. This can help you identify any issues with the containers or the application running inside them.
3. Examine the pod description: Use the command **kubectl describe pod <pod-name>** to get more information about the pod's configuration and status. Look for any events or messages that might indicate the cause of the problem.
4. Check the container runtime: Ensure that the container runtime (e.g., Docker, containerd) is running and functioning correctly on the affected node. You can check the status of the container runtime by running the appropriate command, such as systemctl status docker for Docker.
5. Inspect the network configuration: Verify that the network configuration for the pod and the cluster is correct. This can include checking the network plugin, the Container Network Interface (CNI), and any related configurations.
6. Review Kubernetes events: Use the command **kubectl get events --sort-by='.metadata.creationTimestamp'** to check for any recent events in the cluster that might be related to the issue.
7. Analyze the kubelet logs: The kubelet is the agent that runs on each node and communicates with the Kubernetes API server. Check the kubelet logs on the affected node for any errors or warnings related to the pod sandbox creation. You can usually find the kubelet logs in /var/log/syslog or /var/log/kubelet.log.
8. Verify resource limits: Ensure that the node has sufficient resources (CPU, memory, and storage) to create the pod sandbox. You can use the command **kubectl describe node <node-name>** to check the available resources on the node.
By following these troubleshooting steps, you should be able to identify and resolve the cause of the CreatePodSandboxError. Remember that the specific solution will depend on the underlying issue, so it's essential to carefully review all the information gathered during the troubleshooting process.

https://www.airplane.dev/blog/troubleshooting-failed-to-create-pod-sandbox-error

Case one: Problem with CNI on worker node

        kubectl label node control-plane weave=yes
        kubectl edit ds <name> -n kube-system

Add nodeSelector in spec.template.spec:

        spec:
            nodeSelector:
                weave=yes

#### Runtime errors ConfigPodSandboxError and KillPodSandboxError

ConfigPodSandboxError in Kubernetes usually occurs when there's an issue with the container runtime, network configuration, or other related components. To troubleshoot this error, follow these steps:
1. Check the error message: **kubectl describe pod <pod_name> -n <namespace> ** This will provide you with detailed information about the pod, including the error message.
2. Verify container runtime:
Ensure that the container runtime (e.g., Docker, containerd, or CRI-O) is running and healthy. You can check the status of the container runtime using the following commands: For Docker: **sudo systemctl status docker** For containerd: **sudo systemctl status containerd** For CRI-O: **sudo systemctl status crioIf** the container runtime is not running, start it using the appropriate command, such as sudo systemctl start docker.
3. Check Kubernetes components: 
Verify that all Kubernetes components, such as **kubelet** and **kube-proxy**, are running and healthy. You can check their status using the following command: **sudo systemctl status kubelet**
**sudo systemctl status kube-proxyIf** any of these components are not running, start them using the appropriate command, such as **sudo systemctl start kubelet**.
4. Inspect kubelet logs:
Check the kubelet logs for any errors or issues. You can view the logs using the following command: **sudo journalctl -u kubelet** Look for any error messages or issues related to the pod sandbox, container runtime, or network configuration.
5. Check network configuration:
Ensure that the network plugin (e.g., Calico, Flannel, or Weave) is correctly configured and running. You can check the status of the network plugin using the appropriate command, such as: **kubectl get pods -n kube-system -l k8s-app=calico-node** 
6. Validate pod configuration:
Check the pod's configuration (e.g., YAML file) for any errors or misconfigurations. Ensure that the container image, resources, and other settings are correct.
7. Restart the affected components:
If you've identified any issues with the container runtime, network plugin, or Kubernetes components, try restarting them to see if the issue is resolved.
8. Check for known issues:
Search for any known issues related to the error message or the specific Kubernetes version you are using. There might be a bug or a known issue that can be resolved by upgrading or applying a patch.
By following these steps, you should be able to identify and resolve the ConfigPodSandboxError in Kubernetes. Remember that the error message and logs are your best friends in understanding the root cause of the issue.

