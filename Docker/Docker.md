# Docker

## Docker alpine image with aws and k8s tools

    FROM alpine:3.18.4
    RUN apk --no-cach add curl openssl binutils aws-cli envsubst make git jq bash terraform
    # Install aws authenticator
    RUN curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.6.11/aws-iam-authenticator_0.6.11_linux_amd64 && \
        chmod +x ./aws-iam-authenticator && \
        cp ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator
    # Install kubectl
    RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
        install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
        curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && \
        chmod 700 get_helm.sh && \
        ./get_helm.sh
    # Install helmfile
    RUN curl -LO  https://github.com/helmfile/helmfile/releases/download/v0.158.1/helmfile_0.158.1_linux_amd64.tar.gz && \
        tar xzvf ./helmfile_0.158.1_linux_amd64.tar.gz && \
        chmod 700 helmfile && \
        cp ./helmfile /usr/local/bin/helmfile
    CMD ["/bin/sh"]

## Kaniko build

    # Build idocker image with tools
    stages:
    - build

    build_image:
    stage: build
    image: 
        name: gcr.io/kaniko-project/executor:v1.14.0-debug
        entrypoint: [""]
    before_script:
        # Write credentials to access Gitlab Container Registry within the runner/ci
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_JOB_TOKEN} | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    script:
        - echo "Start create docker image of tools for aws, k3s, kubectl, helm Kaniko"
        - /kaniko/executor
        --registry-mirror "dockerhub.devops.telekom.de"
        --context "${CI_PROJECT_DIR}"
        --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
        --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
        - echo "End create docker image"
    #  rules:
    #    - if: $CI_COMMIT_TAG
