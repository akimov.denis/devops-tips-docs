# Systemd

## links to documentation
- [Debian systemd CheatSheet](https://wiki.debian.org/systemd/CheatSheet)
- [openSUSE:Cheat sheet](https://en.opensuse.org/openSUSE:Cheat_sheet_13.1#Services)
- [Fedora SysVinit to Systemd Cheatsheet](https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet)
- [Oracle Linux 9 systemd](https://docs.oracle.com/en/operating-systems/oracle-linux/9/osmanage/osmanage-WorkingWithSystemServices.html#topic_uqb_zgj_g5b)

## How to create service for systemd

1. Create in /etc/systemd/system file named name-service.service and include the following:

```
[Unit]
Description=<description of the service>

[Service]
User=<User that start service e.g. root>
WorkingDirectory=<working directory of the script e.g. /opt/name-of-service/>
ExecStart=<script/binary which needs to be executed>
# optional items below
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

For python services:

```
[Unit]
Description=<project description>

[Service]
User=<user e.g. root>
WorkingDirectory=<path to your project directory containing your python script>
ExecStart=/home/user/.virtualenv/bin/python main.py
# Or ExecStart=/bin/bash -c 'cd /home/ubuntu/project/ && source venv/bin/activate && python test.py'
# optional items below
Restart=always
RestartSec=3
# replace /home/user/.virtualenv/bin/python with your virtualenv and main.py with your script

[Install]
WantedBy=multi-user.target
```

description of each configuration:

- Description – is used to specify a description for the service.
- After – defines a relationship with a second unit, the network.target. In this case, the test-app.service is activated after the network.target unit.
- User – is used to specifying the user with whose permissions the service will run.
- Group – is used to specify the group with whose permissions the service will run.
- WorkingDirectory – is used to set the working directory for executed processes.
- Environment – is used to set environment variables for executed processes.
- ExecStart – is used to define the commands with their arguments that are executed when this service is started.
- ExecReload – is used to define the commands to execute to trigger a configuration reload in the service.
- WantedBy – enables a symbolic link to be created in the .wants/ or .requires/ directory of each of the listed unit(s), multi-user.target in this case, when the test-app.service unit is enabled using the systemctl enable command.

More information [Service systemd Linux handbook](https://linuxhandbook.com/create-systemd-services/)

2. Reload the service files to include the new service.

   sudo systemctl daemon-reload

3. Start your service

    sudo systemctl start name-of-service.service

4. To check the status of your service

    sudo systemctl status name-of-service.service

5. To enable your service on every reboot

   sudo systemctl enable name-of-service.service

6. To disable your service on every reboot

   sudo systemctl disable name-of-service.service

7. Inspect service logs

```
$ journalctl --user -u user_service
# ...
Jan 12 19:50:20 ubuntu systemd[1511]: Stopped Script Daemon For Test User Services.
Jan 12 19:50:24 ubuntu systemd[1511]: Started Script Daemon For Test User Services.
```

8.  Toggling Services of All Users

With the root privilege, we can enable or disable the service for all users with the global option of systemctl:

```
$ sudo systemctl --global enable user_service.service
Created symlink /etc/systemd/user/default.target.wants/user_service.service → /etc/systemd/user/user_service.service.
```
9. Get list of all services

```
sudo systemctl list-units --type target [--all]
```

10. Shutting Down, Suspending, and Rebooting the System

- systemctl halt : Halt the system.
- systemctl hibernate : Put the system into hibernation.
- systemctl hybrid-sleep : Put the system into hibernation and suspend its operation.
- systemctl poweroff : Halt and power off the system.
- systemctl reboot : Reboot the system.
- systemctl suspend : Suspend the system.

11. cgroups

A cgroup is a collection of processes that are bound together so that you can control their access to system resources. In the example, the cgroup for the httpd service is httpd.service, which is in the system slice.

Slices divide the cgroups on a system into different categories. To display the slice and cgroup hierarchy, use the systemd-cgls command:

```
sudo systemd-cgls

Control group /:
-.slice
├─user.slice
│ └─user-1000.slice
│   ├─user@1000.service
│   │ └─init.scope
│   │   ├─6488 /usr/lib/systemd/systemd --user
│   │   └─6492 (sd-pam)
│   └─session-7.scope
│     ├─6484 sshd: root [priv]
│     ├─6498 sshd: root@pts/0
│     ├─6499 -bash
│     ├─6524 sudo systemd-cgls
│     ├─6526 systemd-cgls
│     └─6527 less
├─init.scope
│ └─1 /usr/lib/systemd/systemd --switched-root --system --deserialize 16
└─system.slice
  ├─rngd.service
  │ └─1266 /sbin/rngd -f --fill-watermark=0
  ├─irqbalance.service
  │ └─1247 /usr/sbin/irqbalance --foreground
  ├─libstoragemgmt.service
  │ └─1201 /usr/bin/lsmd -d
  ├─systemd-udevd.service
  │ └─1060 /usr/lib/systemd/systemd-udevd
  ├─polkit.service
  │ └─1241 /usr/lib/polkit-1/polkitd --no-debug
  ├─chronyd.service
  │ └─1249 /usr/sbin/chronyd
  ├─auditd.service
  │ ├─1152 /sbin/auditd
  │ └─1154 /usr/sbin/sedispatch
  ├─tuned.service
  │ └─1382 /usr/libexec/platform-python -Es /usr/sbin/tuned -l -P
  ├─systemd-journald.service
  │ └─1027 /usr/lib/systemd/systemd-journald
  ├─atd.service
  │ └─1812 /usr/sbin/atd -f
  ├─sshd.service
  │ └─1781 /usr/sbin/sshd
```

12. Running systemctl on a Remote System

```
sudo systemctl -H root@10.0.0.2 status sshd
```

13. Using Timer Units to Control Service Unit Runtime

Timer units can be configured to control when service units run. You can use timer units instead of configuring the cron daemon for time-based events. Timer units can be more complicated to configure than creating a crontab entry. However, timer units are more configurable and the services that they control can be configured for better logging and deeper integration with systemd architecture.

Timer units are started, enabled, and stopped similarly to service units. For example, to enable and start a timer unit immediately, type:

```
sudo systemctl enable --now myscript.timer
```

List:

```
systemctl list-timers
```

Configuring a Realtime Timer Unit:

Create the /etc/systemd/system/update.timer with the following content:

```
[Unit]
Description="Run the update.service every two hours from Mon to Fri."

[Timer]
OnCalendar=Mon..Fri 00/2 
Unit=update.service

[Install]
WantedBy=multi-user.target
```

Check that all the files related to this timer are configured correctly.

    systemd-analyze verify /etc/systemd/system/update.*

Start the timer.

    sudo systemctl start update.timer