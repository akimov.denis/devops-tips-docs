# Lab02

## Borg

- [Large-scale cluster management at Google with Borg](https://storage.googleapis.com/gweb-research2023-media/pubtools/pdf/43438.pdf)

## CRI-O vs containerd
- [Performance Evaluation of Container Runtimes](https://www.scitepress.org/Papers/2020/93404/93404.pdf)

Table 1 shows the summary results of all the benchmarks run in the scope of the study with the highlighted ones being the best for that particular benchmark. To summarize, containerd performs better
when running a container without limits due to lower
runtime overhead, although CRI-O is faster when running a container that has been created beforehand.
The lead of containerd continues in memory performance. Disk performance gives a different picture:
although containerd takes an upper hand in read performance, its superiority is broken by CRI-O in file
read performance

## Kubernetes resurces

- kubernetes.io
- https://github.com/kubernetes/community
- https://stackoverflow.com/search?q=kubernetes
- https://communityinviter.com/apps/kubernetes/community
- https://www.kubernetes.dev/resources/calendar/
- https://github.com/kubernetes/kubernetes/issues

## knowledge dump on container runtimes

KataContainers
- image coupled with kernel 
- light vm layer
- can run in nested virturalization environments if hardware supports and you can enable it in bios (ex. only bare metal EC2 instances, limits many cloud providers)
- slower startup time
- OCI compliant
- previously known as ClearContainers by Intel

gvisor
- kernel implemented in userspace
- layer between container and kernel, intercepts syscalls 
- quicker to cover kernel vulnerabilities as soon they discovered 
- 211 of the 319 x86-64 system calls implemented, using only 64 system calls in the host system 
- limited syscall implementations (relying on community)
- faster startup time than kata
- OCI compliant
- By google, used in production for a few years

runc
- usually the default container runtime
- relies on seccomp, selinux, or apparmor for security policies (syscall filtering, difficult to get it right)
- first runtime to be OCI compliant
- docker built an abstraction layer over `lxc` called `libcontainer` which now it’s called `runc`

rkt (rocket)
- a layer on top runc (more user-friendly)
- non OCI compliant
- by CoreOS

NablaContainers 
- competitor to gvisor
- Uses only 9 syscalls (blocks all others with seccomp policy)
- uses the solo5 project which implements syscall functionality
- can't run linux containers out of the box
- claims to be more secure than gvisor
- by IBM 

containerd (container daemon)
- container runtime that manages container lifecycle (image transfer/pull/push, supervision, networking, etc)
- default to runc runtime
- runs OCI compliant images

cri-o (Container Runtime Interface)
- runtime created specifically for kubernetes (like containerd)
- defaults to runc runtime
- runs OCI compliant images

cri-containerd
- containerd daemon which implemented the cri-o interface (can use containerd with kubernetes)