# Lessons 04

## Kubernetes Folder Structure

```
/etc/kubernetes/
├── manifests/                       # YAML files defining Kubernetes resources
│   ├── etcd.yaml                    # etcd cluster manifest
│   ├── kube-apiserver.yaml          # kube-apiserver manifest
│   ├── kube-controller-manager.yaml # kube-controller-manager manifest
│   ├── kube-scheduler.yaml          # kube-scheduler manifest
│   ├── kube-proxy.yaml              # kube-proxy manifest
│   └── ...
├── pki/                             # Public Key Infrastructure for Kubernetes
│   ├── apiserver.crt                # Kubernetes API Server certificate
│   ├── apiserver.key                # Kubernetes API Server private key
│   ├── ca.crt                       # Cluster Certificate Authority certificate
│   ├── ca.key                       # Cluster Certificate Authority private key
│   └── ...
├── kubelet.conf                     # kubelet configuration file
├── controller-manager.conf          # kube-controller-manager configuration file
├── scheduler.conf                   # kube-scheduler configuration file
└── admin.conf                       # kubeconfig file for cluster administrator

/etc/cni/net.d/                      # Folder containing CNI configuration files

/var/lib/kubelet/
├── pods/                            # Directory containing pod volumes and data
├── pki/                             # Public Key Infrastructure for kubelet
├── config.yaml                      # kubelet manifest
└── ...

/opt/cni/bin/                        # Container Networking Interface (CNI) binaries
```

## Illustrated Guide To Kubernetes Networking

https://speakerdeck.com/thockin/illustrated-guide-to-kubernetes-networking?slide=11

# Lab 4.1

## Backup etcd
Get data dir:

    vagrant@cp:~$ sudo grep data-dir /etc/kubernetes/manifests/etcd.yaml
        - --data-dir=/var/lib/etcd

Login in etcd conteiner:

    vagrant@cp:~$ kubectl -n kube-system exec -it etcd-cp -- sh

etcdctl:

```
vagrant@cp:~$ kubectl -n kube-system exec -it etcd-cp -- sh
sh-5.1# etcdctl -h
NAME:
        etcdctl - A simple command line client for etcd3.

USAGE:
        etcdctl [flags]

VERSION:
        3.5.7

API VERSION:
        3.5


COMMANDS:
        alarm disarm            Disarms all alarms
        alarm list              Lists all alarms
        auth disable            Disables authentication
        auth enable             Enables authentication
        auth status             Returns authentication status
        check datascale         Check the memory usage of holding data for different workloads on a given server endpoint.
        check perf              Check the performance of the etcd cluster
        compaction              Compacts the event history in etcd
        defrag                  Defragments the storage of the etcd members with given endpoints
        del                     Removes the specified key or range of keys [key, range_end)
        elect                   Observes and participates in leader election
        endpoint hashkv         Prints the KV history hash for each endpoint in --endpoints
        endpoint health         Checks the healthiness of endpoints specified in `--endpoints` flag
        endpoint status         Prints out the status of endpoints specified in `--endpoints` flag
        get                     Gets the key or a range of keys
        help                    Help about any command
        lease grant             Creates leases
        lease keep-alive        Keeps leases alive (renew)
        lease list              List all active leases
        lease revoke            Revokes leases
        lease timetolive        Get lease information
        lock                    Acquires a named lock
        make-mirror             Makes a mirror at the destination etcd cluster
        member add              Adds a member into the cluster
        member list             Lists all members in the cluster
        member promote          Promotes a non-voting member in the cluster
        member remove           Removes a member from the cluster
        member update           Updates a member in the cluster
        move-leader             Transfers leadership to another etcd cluster member.
        put                     Puts the given key into the store
        role add                Adds a new role
        role delete             Deletes a role
        role get                Gets detailed information of a role
        role grant-permission   Grants a key to a role
        role list               Lists all roles
        role revoke-permission  Revokes a key from a role
        snapshot restore        Restores an etcd member snapshot to an etcd directory
        snapshot save           Stores an etcd node backend snapshot to a given file
        snapshot status         [deprecated] Gets backend snapshot status of a given file
        txn                     Txn processes all the requests in one transaction
        user add                Adds a new user
        user delete             Deletes a user
        user get                Gets detailed information of a user
        user grant-role         Grants a role to a user
        user list               Lists all users
        user passwd             Changes password of user
        user revoke-role        Revokes a role from a user
        version                 Prints the version of etcdctl
        watch                   Watches events stream on keys or prefixes

OPTIONS:
      --cacert=""                               verify certificates of TLS-enabled secure servers using this CA bundle
      --cert=""                                 identify secure client using this TLS certificate file
      --command-timeout=5s                      timeout for short running command (excluding dial timeout)
      --debug[=false]                           enable client-side debug logging
      --dial-timeout=2s                         dial timeout for client connections
  -d, --discovery-srv=""                        domain name to query for SRV records describing cluster endpoints
      --discovery-srv-name=""                   service name to query when using DNS discovery
      --endpoints=[127.0.0.1:2379]              gRPC endpoints
  -h, --help[=false]                            help for etcdctl
      --hex[=false]                             print byte strings as hex encoded strings
      --insecure-discovery[=true]               accept insecure SRV records describing cluster endpoints
      --insecure-skip-tls-verify[=false]        skip server certificate verification (CAUTION: this option should be enabled only for testing purposes)
      --insecure-transport[=true]               disable transport security for client connections
      --keepalive-time=2s                       keepalive time for client connections
      --keepalive-timeout=6s                    keepalive timeout for client connections
      --key=""                                  identify secure client using this TLS key file
      --password=""                             password for authentication (if this option is used, --user option shouldn't include password)
      --user=""                                 username[:password] for authentication (prompt if password is not supplied)
  -w, --write-out="simple"                      set the output format (fields, json, protobuf, simple, table)

```
Certificates for TLS:

```
sh-5.1# cd /etc/kubernetes/pki/etcd
sh-5.1# echo *
ca.crt ca.key healthcheck-client.crt healthcheck-client.key peer.crt peer.key server.crt server.key

```

Check the health of the database:

```
kubectl -n kube-system exec -it etcd-cp -- sh \
-c "ETCDCTL_API=3 \
ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
etcdctl endpoint health"
```

Output:

```
vagrant@cp:~$ kubectl -n kube-system exec -it etcd-cp -- sh \
> -c "ETCDCTL_API=3 \
> ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
> ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
> ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
> etcdctl endpoint health"
127.0.0.1:2379 is healthy: successfully committed proposal: took = 64.170703ms
```

List member:

```
kubectl -n kube-system exec -it etcd-cp -- sh \
-c "ETCDCTL_API=3 \
ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
etcdctl --endpoints=https://127.0.0.1:2379 member list"
```

Status in table:

```
vagrant@cp:~$ kubectl -n kube-system exec -it etcd-cp -- sh \
> -c "ETCDCTL_API=3 \
> ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
> ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
> ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
> etcdctl --endpoints=https://127.0.0.1:2379 member list -w table"
+------------------+---------+------+-------------------------+-------------------------+------------+
|        ID        | STATUS  | NAME |       PEER ADDRS        |      CLIENT ADDRS       | IS LEARNER |
+------------------+---------+------+-------------------------+-------------------------+------------+
| afd6d30120c56006 | started |   cp | https://10.0.0.175:2380 | https://10.0.0.175:2379 |      false |
+------------------+---------+------+-------------------------+-------------------------+------------+

```

Backup etcd, make snapshot:

```
kubectl -n kube-system exec -it etcd-cp -- sh \
-c "ETCDCTL_API=3 \
ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
etcdctl --endpoints=https://127.0.0.1:2379 snapshot save /var/lib/etcd/snapshot.db"
```

Output:

```
vagrant@cp:~$ kubectl -n kube-system exec -it etcd-cp -- sh \
> -c "ETCDCTL_API=3 \
> ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
> ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
> ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
> etcdctl --endpoints=https://127.0.0.1:2379 snapshot save /var/lib/etcd/snapshot.db"
{"level":"info","ts":"2024-03-15T20:04:03.695Z","caller":"snapshot/v3_snapshot.go:65","msg":"created temporary db file","path":"/var/lib/etcd/snapshot.db.part"}
{"level":"info","ts":"2024-03-15T20:04:03.762Z","logger":"client","caller":"v3@v3.5.7/maintenance.go:212","msg":"opened snapshot stream; downloading"}
{"level":"info","ts":"2024-03-15T20:04:03.762Z","caller":"snapshot/v3_snapshot.go:73","msg":"fetching snapshot","endpoint":"https://127.0.0.1:2379"}
{"level":"info","ts":"2024-03-15T20:04:04.179Z","logger":"client","caller":"v3@v3.5.7/maintenance.go:220","msg":"completed snapshot read; closing"}
{"level":"info","ts":"2024-03-15T20:04:04.196Z","caller":"snapshot/v3_snapshot.go:88","msg":"fetched snapshot","endpoint":"https://127.0.0.1:2379","size":"4.8 MB","took":"now"}
{"level":"info","ts":"2024-03-15T20:04:04.196Z","caller":"snapshot/v3_snapshot.go:97","msg":"saved","path":"/var/lib/etcd/snapshot.db"}
Snapshot saved at /var/lib/etcd/snapshot.db

```

Check snapshot:

```
vagrant@cp:~$ sudo ls -l /var/lib/etcd/
total 4648
drwx------ 4 root root    4096 Mar 15 18:56 member
-rw------- 1 root root 4751392 Mar 15 20:04 snapshot.db
```

Copy:

```
mkdir $HOME/backup
sudo cp /var/lib/etcd/snapshot.db $HOME/backup/snapshot.db-$(date +%m-%d-%y)
sudo cp /root/kubeadm-config.yaml $HOME/backup/
sudo cp -r /etc/kubernetes/pki/etcd $HOME/backup/
```

## Upgrade the Cluster

Chenge repository:

```
echo "deb [signed-by=/etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg] \
https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /" \
| sudo tee /etc/apt/sources.list.d/kubernetes.list

curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key \
| sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg
```

```
vagrant@cp:~$ echo "deb [signed-by=/etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg] \
> https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /" \
> | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb [signed-by=/etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /
vagrant@cp:~$ sudo cat /etc/apt/sources.list.d/kubernetes.list
deb [signed-by=/etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /
vagrant@cp:~$

vagrant@cp:~$ curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key \
> | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg
vagrant@cp:~$ sudo ls /etc/apt/trusted.gpg.d/
kubernetes-apt-keyring.gpg       ubuntu-keyring-2012-cdimage.gpg
ubuntu-keyring-2012-archive.gpg  ubuntu-keyring-2018-archive.gpg
```

Check packages:

```
vagrant@cp:~$ sudo apt-cache madison kubeadm
   kubeadm | 1.29.3-1.1 | https://pkgs.k8s.io/core:/stable:/v1.29/deb  Packages
   kubeadm | 1.29.2-1.1 | https://pkgs.k8s.io/core:/stable:/v1.29/deb  Packages
   kubeadm | 1.29.1-1.1 | https://pkgs.k8s.io/core:/stable:/v1.29/deb  Packages
   kubeadm | 1.29.0-1.1 | https://pkgs.k8s.io/core:/stable:/v1.29/deb  Packages
```

Unhold kubeadm:

    sudo apt-mark unhold kubeadm
    sudo apt-get install -y kubeadm=1.29.1-1.1
    sudo apt-mark hold kubeadm

```
vagrant@cp:~$ sudo apt-mark unhold kubeadm
Canceled hold on kubeadm.
vagrant@cp:~$ sudo apt-get install -y kubeadm=1.29.1-1.1
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  cri-tools
The following packages will be upgraded:
  cri-tools kubeadm
2 upgraded, 0 newly installed, 0 to remove and 32 not upgraded.
Need to get 30.2 MB of archives.
After this operation, 2925 kB of additional disk space will be used.
Get:1 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.29/deb  cri-tools 1.29.0-1.1 [20.1 MB]
Get:2 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.29/deb  kubeadm 1.29.1-1.1 [10.1 MB]
Fetched 30.2 MB in 3s (9973 kB/s)
(Reading database ... 63969 files and directories currently installed.)
Preparing to unpack .../cri-tools_1.29.0-1.1_amd64.deb ...
Unpacking cri-tools (1.29.0-1.1) over (1.26.0-00) ...
Preparing to unpack .../kubeadm_1.29.1-1.1_amd64.deb ...
Unpacking kubeadm (1.29.1-1.1) over (1.27.1-00) ...
dpkg: warning: unable to delete old directory '/etc/systemd/system/kubelet.service.d': Directory not empty
Setting up cri-tools (1.29.0-1.1) ...
Setting up kubeadm (1.29.1-1.1) ...
vagrant@cp:~$ sudo apt-mark hold kubeadm
kubeadm set on hold.
vagrant@cp:~$
vagrant@cp:~$ sudo kubeadm version
kubeadm version: &version.Info{Major:"1", Minor:"29", GitVersion:"v1.29.1", GitCommit:"bc401b91f2782410b3fb3f9acf43a995c4de90d2", GitTreeState:"clean", BuildDate:"2024-01-17T15:49:02Z", GoVersion:"go1.21.6", Compiler:"gc", Platform:"linux/amd64"}
vagrant@cp:~$


```

Drain cp node:

```
vagrant@cp:~$ kubectl get node
NAME   STATUS   ROLES           AGE   VERSION
cp     Ready    control-plane   36d   v1.27.1
wrk1   Ready    <none>          36d   v1.27.1
vagrant@cp:~$ kubectl drain cp --ignore-daemonsets
node/cp cordoned
Warning: ignoring DaemonSet-managed Pods: kube-system/cilium-kks4s, kube-system/kube-proxy-wg5rm
evicting pod kube-system/cilium-operator-788c7d7585-cvbwq
pod/cilium-operator-788c7d7585-cvbwq evicted
node/cp drained

```

Upgrade plan:

```
vagrant@cp:~$ sudo kubeadm upgrade plan
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[upgrade/config] FATAL: this version of kubeadm only supports deploying clusters with the control plane version >= 1.28.0. Current version: v1.27.1
To see the stack trace of this error execute with --v=5 or higher

vagrant@cp:~$ sudo kubeadm upgrade plan --v=5
I0315 22:06:03.932970   16284 plan.go:248] [upgrade/plan] verifying health of cluster
I0315 22:06:03.933047   16284 plan.go:249] [upgrade/plan] retrieving configuration from cluster
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
I0315 22:06:03.983833   16284 kubelet.go:196] the value of KubeletConfiguration.cgroupDriver is empty; setting it to "systemd"
this version of kubeadm only supports deploying clusters with the control plane version >= 1.28.0. Current version: v1.27.1
k8s.io/kubernetes/cmd/kubeadm/app/util/config.NormalizeKubernetesVersion
        cmd/kubeadm/app/util/config/common.go:156
k8s.io/kubernetes/cmd/kubeadm/app/util/config.SetClusterDynamicDefaults
        cmd/kubeadm/app/util/config/initconfiguration.go:175
k8s.io/kubernetes/cmd/kubeadm/app/util/config.SetInitDynamicDefaults
        cmd/kubeadm/app/util/config/initconfiguration.go:70
k8s.io/kubernetes/cmd/kubeadm/app/util/config.FetchInitConfigurationFromCluster
        cmd/kubeadm/app/util/config/cluster.go:63
k8s.io/kubernetes/cmd/kubeadm/app/cmd/upgrade.loadConfig
        cmd/kubeadm/app/cmd/upgrade/common.go:73
k8s.io/kubernetes/cmd/kubeadm/app/cmd/upgrade.enforceRequirements
        cmd/kubeadm/app/cmd/upgrade/common.go:137
k8s.io/kubernetes/cmd/kubeadm/app/cmd/upgrade.runPlan
        cmd/kubeadm/app/cmd/upgrade/plan.go:250
k8s.io/kubernetes/cmd/kubeadm/app/cmd/upgrade.newCmdPlan.func1
        cmd/kubeadm/app/cmd/upgrade/plan.go:66
github.com/spf13/cobra.(*Command).execute
        vendor/github.com/spf13/cobra/command.go:940
github.com/spf13/cobra.(*Command).ExecuteC
        vendor/github.com/spf13/cobra/command.go:1068
github.com/spf13/cobra.(*Command).Execute
        vendor/github.com/spf13/cobra/command.go:992
k8s.io/kubernetes/cmd/kubeadm/app.Run
        cmd/kubeadm/app/kubeadm.go:50
main.main
        cmd/kubeadm/kubeadm.go:25
runtime.main
        /usr/local/go/src/runtime/proc.go:267
runtime.goexit
        /usr/local/go/src/runtime/asm_amd64.s:1650
[upgrade/config] FATAL
k8s.io/kubernetes/cmd/kubeadm/app/cmd/upgrade.enforceRequirements
        cmd/kubeadm/app/cmd/upgrade/common.go:150
k8s.io/kubernetes/cmd/kubeadm/app/cmd/upgrade.runPlan
        cmd/kubeadm/app/cmd/upgrade/plan.go:250
k8s.io/kubernetes/cmd/kubeadm/app/cmd/upgrade.newCmdPlan.func1
        cmd/kubeadm/app/cmd/upgrade/plan.go:66
github.com/spf13/cobra.(*Command).execute
        vendor/github.com/spf13/cobra/command.go:940
github.com/spf13/cobra.(*Command).ExecuteC
        vendor/github.com/spf13/cobra/command.go:1068
github.com/spf13/cobra.(*Command).Execute
        vendor/github.com/spf13/cobra/command.go:992
k8s.io/kubernetes/cmd/kubeadm/app.Run
        cmd/kubeadm/app/kubeadm.go:50
main.main
        cmd/kubeadm/kubeadm.go:25
runtime.main
        /usr/local/go/src/runtime/proc.go:267
runtime.goexit
        /usr/local/go/src/runtime/asm_amd64.s:1650
vagrant@cp:~$

```

Lab was changed upgrade firs on 1.28


```
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg]\
https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' \
| sudo tee /etc/apt/sources.list.d/kubernetes.list

vagrant@cp:~$ sudo apt-mark unhold kubeadm
Canceled hold on kubeadm.
vvagrant@cp:~$ sudo apt-get install  --allow-downgrades kubeadm=1.28.1-1.1
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following held packages will be changed:
  kubeadm
The following packages will be DOWNGRADED:
  kubeadm
0 upgraded, 0 newly installed, 1 downgraded, 0 to remove and 32 not upgraded.
Need to get 10.3 MB of archives.
After this operation, 2503 kB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  kubeadm 1.28.1-1.1 [10.3 MB]
Fetched 10.3 MB in 1s (9454 kB/s)
dpkg: warning: downgrading kubeadm from 1.29.1-1.1 to 1.28.1-1.1
(Reading database ... 63976 files and directories currently installed.)
Preparing to unpack .../kubeadm_1.28.1-1.1_amd64.deb ...
Unpacking kubeadm (1.28.1-1.1) over (1.29.1-1.1) ...
Setting up kubeadm (1.28.1-1.1) ...


vagrant@cp:~$ sudo kubeadm upgrade plan
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade] Fetching available versions to upgrade to
[upgrade/versions] Cluster version: v1.27.1
[upgrade/versions] kubeadm version: v1.28.1
I0315 22:23:29.792540   17871 version.go:256] remote version is much newer: v1.29.3; falling back to: stable-1.28
[upgrade/versions] Target version: v1.28.8
[upgrade/versions] Latest version in the v1.27 series: v1.27.12

Components that must be upgraded manually after you have upgraded the control plane with 'kubeadm upgrade apply':
COMPONENT   CURRENT       TARGET
kubelet     2 x v1.27.1   v1.27.12

Upgrade to the latest version in the v1.27 series:

COMPONENT                 CURRENT   TARGET
kube-apiserver            v1.27.1   v1.27.12
kube-controller-manager   v1.27.1   v1.27.12
kube-scheduler            v1.27.1   v1.27.12
kube-proxy                v1.27.1   v1.27.12
CoreDNS                   v1.10.1   v1.10.1
etcd                      3.5.7-0   3.5.9-0

You can now apply the upgrade by executing the following command:

        kubeadm upgrade apply v1.27.12

_____________________________________________________________________

Components that must be upgraded manually after you have upgraded the control plane with 'kubeadm upgrade apply':
COMPONENT   CURRENT       TARGET
kubelet     2 x v1.27.1   v1.28.8

Upgrade to the latest stable version:

COMPONENT                 CURRENT   TARGET
kube-apiserver            v1.27.1   v1.28.8
kube-controller-manager   v1.27.1   v1.28.8
kube-scheduler            v1.27.1   v1.28.8
kube-proxy                v1.27.1   v1.28.8
CoreDNS                   v1.10.1   v1.10.1
etcd                      3.5.7-0   3.5.9-0

You can now apply the upgrade by executing the following command:

        kubeadm upgrade apply v1.28.8

Note: Before you can perform this upgrade, you have to update kubeadm to v1.28.8.

_____________________________________________________________________


The table below shows the current state of component configs as understood by this version of kubeadm.
Configs that have a "yes" mark in the "MANUAL UPGRADE REQUIRED" column require manual config upgrade or
resetting to kubeadm defaults before a successful upgrade can be performed. The version to manually
upgrade to is denoted in the "PREFERRED VERSION" column.

API GROUP                 CURRENT VERSION   PREFERRED VERSION   MANUAL UPGRADE REQUIRED
kubeproxy.config.k8s.io   v1alpha1          v1alpha1            no
kubelet.config.k8s.io     v1beta1           v1beta1             no
_____________________________________________________________________

vagrant@cp:~$ sudo kubeadm upgrade apply v1.28.1
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade/version] You have chosen to change the cluster version to "v1.28.1"
[upgrade/versions] Cluster version: v1.27.1
[upgrade/versions] kubeadm version: v1.28.1
[upgrade] Are you sure you want to proceed? [y/N]: y
[upgrade/prepull] Pulling images required for setting up a Kubernetes cluster
[upgrade/prepull] This might take a minute or two, depending on the speed of your internet connection
[upgrade/prepull] You can also perform this action in beforehand using 'kubeadm config images pull'
W0315 22:28:31.707260   18127 checks.go:835] detected that the sandbox image "registry.k8s.io/pause:3.8" of the container runtime is inconsistent with that used by kubeadm. It is recommended that using "registry.k8s.io/pause:3.9" as the CRI sandbox image.
[upgrade/apply] Upgrading your Static Pod-hosted control plane to version "v1.28.1" (timeout: 5m0s)...
[upgrade/etcd] Upgrading to TLS for etcd
[upgrade/staticpods] Preparing for "etcd" upgrade
[upgrade/staticpods] Renewing etcd-server certificate
[upgrade/staticpods] Renewing etcd-peer certificate
[upgrade/staticpods] Renewing etcd-healthcheck-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/etcd.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2024-03-15-22-29-00/etcd.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
[apiclient] Found 1 Pods for label selector component=etcd
[upgrade/staticpods] Component "etcd" upgraded successfully!
[upgrade/etcd] Waiting for etcd to become available
[upgrade/staticpods] Writing new Static Pod manifests to "/etc/kubernetes/tmp/kubeadm-upgraded-manifests2286646166"
[upgrade/staticpods] Preparing for "kube-apiserver" upgrade
[upgrade/staticpods] Renewing apiserver certificate
[upgrade/staticpods] Renewing apiserver-kubelet-client certificate
[upgrade/staticpods] Renewing front-proxy-client certificate
[upgrade/staticpods] Renewing apiserver-etcd-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-apiserver.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2024-03-15-22-29-00/kube-apiserver.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
[apiclient] Found 1 Pods for label selector component=kube-apiserver
[upgrade/staticpods] Component "kube-apiserver" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-controller-manager" upgrade
[upgrade/staticpods] Renewing controller-manager.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-controller-manager.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2024-03-15-22-29-00/kube-controller-manager.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
[apiclient] Found 1 Pods for label selector component=kube-controller-manager
[upgrade/staticpods] Component "kube-controller-manager" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-scheduler" upgrade
[upgrade/staticpods] Renewing scheduler.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-scheduler.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2024-03-15-22-29-00/kube-scheduler.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
[apiclient] Found 1 Pods for label selector component=kube-scheduler
[upgrade/staticpods] Component "kube-scheduler" upgraded successfully!
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config" in namespace kube-system with the configuration for the kubelets in the cluster
[upgrade] Backing up kubelet config file to /etc/kubernetes/tmp/kubeadm-kubelet-config2168456861/config.yaml
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] Configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] Configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

[upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.28.1". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with 
upgrading your kubelets if you haven't already done so.


vagrant@cp:~$ sudo apt-mark unhold kubelet kubect
Canceled hold on kubelet.
E: Unable to locate package kubect

vagrant@cp:~$ sudo apt-get install -y --allow-change-held-packages kubelet=1.28.1-1.1 kubectl=1.28.1-1.1
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following held packages will be changed:
  kubectl
The following packages will be upgraded:
  kubectl kubelet
2 upgraded, 0 newly installed, 0 to remove and 31 not upgraded.
Need to get 29.8 MB of archives.
After this operation, 5341 kB of additional disk space will be used.
Get:1 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  kubectl 1.28.1-1.1 [10.3 MB]
Get:2 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  kubelet 1.28.1-1.1 [19.5 MB]
Fetched 29.8 MB in 2s (13.8 MB/s)
(Reading database ... 63976 files and directories currently installed.)
Preparing to unpack .../kubectl_1.28.1-1.1_amd64.deb ...
Unpacking kubectl (1.28.1-1.1) over (1.27.1-00) ...
Preparing to unpack .../kubelet_1.28.1-1.1_amd64.deb ...
Unpacking kubelet (1.28.1-1.1) over (1.27.1-00) ...
Setting up kubectl (1.28.1-1.1) ...
Setting up kubelet (1.28.1-1.1) ...
vagrant@cp:~$ sudo apt-mark hold kubelet kubectl
kubelet set on hold.
kubectl set on hold.
vagrant@cp:~$ sudo systemctl daemon-reload
vagrant@cp:~$ sudo systemctl restart kubelet
vagrant@cp:~$ kubectl get node
NAME   STATUS                     ROLES           AGE   VERSION
cp     Ready,SchedulingDisabled   control-plane   36d   v1.28.1
wrk1   Ready                      <none>          36d   v1.27.1
vagrant@cp:~$ kubectl uncordon cp
node/cp uncordoned
vagrant@cp:~$ kubectl get node
NAME   STATUS   ROLES           AGE   VERSION
cp     Ready    control-plane   36d   v1.28.1
wrk1   Ready    <none>          36d   v1.27.1

```

Upgrade worker node to 1.28:

```
vagrant@wrk1:~$ echo "deb [signed-by=/etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg] \
> https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /" \
> | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb [signed-by=/etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /
vagrant@wrk1:~$
vagrant@wrk1:~$ curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key \
> | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/kubernetes-apt-keyring.gpg
vagrant@wrk1:~$

vagrant@wrk1:~$ sudo apt-get update
Get:1 http://security.ubuntu.com/ubuntu focal-security InRelease [114 kB]
Hit:2 http://archive.ubuntu.com/ubuntu focal InRelease
Get:3 https://download.docker.com/linux/ubuntu focal InRelease [57.7 kB]
Get:4 http://archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB]
Hit:6 http://archive.ubuntu.com/ubuntu focal-backports InRelease
Get:5 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  InRelease [1189 B]
Get:7 https://download.docker.com/linux/ubuntu focal/stable amd64 Packages [38.8 kB]
Get:8 http://security.ubuntu.com/ubuntu focal-security/main amd64 Packages [2779 kB]
Get:9 http://security.ubuntu.com/ubuntu focal-security/main Translation-en [421 kB]
Get:10 http://security.ubuntu.com/ubuntu focal-security/restricted amd64 Packages [2638 kB]
Get:11 http://security.ubuntu.com/ubuntu focal-security/restricted Translation-en [368 kB]
Get:12 http://security.ubuntu.com/ubuntu focal-security/universe amd64 Packages [950 kB]
Get:13 http://security.ubuntu.com/ubuntu focal-security/universe Translation-en [200 kB]
Get:14 http://security.ubuntu.com/ubuntu focal-security/multiverse amd64 Packages [23.9 kB]
Get:15 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages [3156 kB]
Get:16 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  Packages [12.6 kB]
Get:17 http://archive.ubuntu.com/ubuntu focal-updates/main Translation-en [504 kB]
Get:18 http://archive.ubuntu.com/ubuntu focal-updates/restricted amd64 Packages [2755 kB]
Get:19 http://archive.ubuntu.com/ubuntu focal-updates/restricted Translation-en [385 kB]
Get:20 http://archive.ubuntu.com/ubuntu focal-updates/universe amd64 Packages [1175 kB]
Get:21 http://archive.ubuntu.com/ubuntu focal-updates/universe Translation-en [282 kB]
Get:22 http://archive.ubuntu.com/ubuntu focal-updates/multiverse amd64 Packages [26.1 kB]
Fetched 16.0 MB in 8s (1979 kB/s)
Reading package lists... Done
vagrant@wrk1:~$ sudo apt-cache madison kubeadm
   kubeadm | 1.28.8-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.7-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.6-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.5-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.4-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.3-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.2-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.1-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
   kubeadm | 1.28.0-1.1 | https://pkgs.k8s.io/core:/stable:/v1.28/deb  Packages
vagrant@wrk1:~$


vagrant@wrk1:~$ sudo apt-get update && sudo apt-get install -y kubeadm=1.28.1-1.1
Hit:1 https://download.docker.com/linux/ubuntu focal InRelease
Hit:2 http://security.ubuntu.com/ubuntu focal-security InRelease
Hit:3 http://archive.ubuntu.com/ubuntu focal InRelease
Hit:5 http://archive.ubuntu.com/ubuntu focal-updates InRelease
Hit:6 http://archive.ubuntu.com/ubuntu focal-backports InRelease
Hit:4 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  InRelease
Reading package lists... Done
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  cri-tools
The following packages will be upgraded:
  cri-tools kubeadm
2 upgraded, 0 newly installed, 0 to remove and 31 not upgraded.
Need to get 29.9 MB of archives.
After this operation, 5035 kB of additional disk space will be used.
Get:1 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  cri-tools 1.28.0-1.1 [19.6 MB]
Get:2 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  kubeadm 1.28.1-1.1 [10.3 MB]
Fetched 29.9 MB in 2s (19.1 MB/s)
(Reading database ... 63965 files and directories currently installed.)
Preparing to unpack .../cri-tools_1.28.0-1.1_amd64.deb ...
Unpacking cri-tools (1.28.0-1.1) over (1.26.0-00) ...
Preparing to unpack .../kubeadm_1.28.1-1.1_amd64.deb ...
Unpacking kubeadm (1.28.1-1.1) over (1.27.1-00) ...
dpkg: warning: unable to delete old directory '/etc/systemd/system/kubelet.service.d': Directory not empty
Setting up cri-tools (1.28.0-1.1) ...
Setting up kubeadm (1.28.1-1.1) ...
vagrant@wrk1:~$


vagrant@cp:~$ kubectl drain wrk1 --ignore-daemonsets
node/wrk1 cordoned
Warning: ignoring DaemonSet-managed Pods: kube-system/cilium-xlxbh, kube-system/kube-proxy-6t65p
evicting pod kube-system/coredns-5d78c9869d-jlsxn
evicting pod kube-system/cilium-operator-788c7d7585-pr476
evicting pod default/nginx-77b4fdf86c-pfgzx
evicting pod kube-system/coredns-5d78c9869d-7fphp
pod/cilium-operator-788c7d7585-pr476 evicted
pod/nginx-77b4fdf86c-pfgzx evicted
pod/coredns-5d78c9869d-7fphp evicted
pod/coredns-5d78c9869d-jlsxn evicted
node/wrk1 drained



vagrant@wrk1:~$ sudo kubeadm upgrade node
[upgrade] Reading configuration from the cluster...
[upgrade] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks
[preflight] Skipping prepull. Not a control plane node.
[upgrade] Skipping phase. Not a control plane node.
[upgrade] Backing up kubelet config file to /etc/kubernetes/tmp/kubeadm-kubelet-config3723885346/config.yaml
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[upgrade] The configuration for this node was successfully updated!
[upgrade] Now you should go ahead and upgrade the kubelet package using your package manager.

vagrant@wrk1:~$ sudo apt-get install -y kubelet=1.28.1-1.1 kubectl=1.28.1-1.1
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages will be upgraded:
  kubectl kubelet
2 upgraded, 0 newly installed, 0 to remove and 30 not upgraded.
Need to get 29.8 MB of archives.
After this operation, 5341 kB of additional disk space will be used.
Get:1 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  kubectl 1.28.1-1.1 [10.3 MB]
Get:2 https://prod-cdn.packages.k8s.io/repositories/isv:/kubernetes:/core:/stable:/v1.28/deb  kubelet 1.28.1-1.1 [19.5 MB]
Fetched 29.8 MB in 2s (17.4 MB/s)
(Reading database ... 63972 files and directories currently installed.)
Preparing to unpack .../kubectl_1.28.1-1.1_amd64.deb ...
Unpacking kubectl (1.28.1-1.1) over (1.27.1-00) ...
Preparing to unpack .../kubelet_1.28.1-1.1_amd64.deb ...
Unpacking kubelet (1.28.1-1.1) over (1.27.1-00) ...
Setting up kubectl (1.28.1-1.1) ...
Setting up kubelet (1.28.1-1.1) ...
vagrant@wrk1:~$ sudo apt-mark hold kubelet kubectl
kubelet set on hold.
kubectl set on hold.
vagrant@wrk1:~$ sudo systemctl daemon-reload
vagrant@wrk1:~$ sudo systemctl restart kubelet

vagrant@cp:~$ kubectl get node
NAME   STATUS                     ROLES           AGE   VERSION
cp     Ready                      control-plane   36d   v1.28.1
wrk1   Ready,SchedulingDisabled   <none>          36d   v1.28.1

vagrant@cp:~$ kubectl get node
NAME   STATUS   ROLES           AGE   VERSION
cp     Ready    control-plane   36d   v1.28.1
wrk1   Ready    <none>          36d   v1.28.1

```

Update cp to 1.29:

