# Lesson 03

## Installation metod notes 

### https://github.com/kubernetes-sigs/kubespray

Can be deployed on AWS, GCE, Azure, OpenStack, vSphere, Equinix Metal (bare metal), Oracle Cloud Infrastructure (Experimental), or Baremetal

### Large cluster guide

- https://kubernetes.io/docs/setup/best-practices/cluster-large/#size-of-master-and-master-components

Kubernetes v1.29 supports clusters with up to 5,000 nodes.

No more than 110 pods per node
No more than 5,000 nodes
No more than 150,000 total pods
No more than 300,000 total containers

- https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/setup-ha-etcd-with-kubeadm/

- https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/setup-ha-etcd-with-kubeadm/

## GKE quickstart guide 

https://cloud.google.com/kubernetes-engine/docs/deploy-app-cluster

        $ gcloud container clusters create linuxfoundation
        $ gcloud container clusters list
        $ kubectl get nodes
        $ gcloud container clusters delete linuxfoundation

## Kubeadm

- https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/
- https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/troubleshooting-kubeadm/

    kubeadm init
    kubeadm join
    kubectl create -f ht‌tps://git.io/weave-kube

## weave-kube

[Kubernetes CNI pluggins](image-1.png)

- https://www.techtarget.com/searchitoperations/tutorial/Step-by-step-guide-Get-started-with-Weave-for-Kubernetes


Service discovery. This helps containers identify each other on the network.
Load balancing. This distributes network load efficiently among containers.
Fault resilience. This minimizes network errors.
Multicast support. This lets users broadcast a single packet to multiple recipients in an efficient and simple way.
Network address translation (NAT) traversal. This more easily routes traffic between resources without public IP addresses.

## kubeadn-upgrade

- https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/


## Kubernetes The Hard Way

- https://github.com/kelseyhightower/kubernetes-the-hard-way

