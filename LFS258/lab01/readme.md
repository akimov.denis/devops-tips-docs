# DRAFT Environment for lab

Based on https://github.com/hybby/cheapk8s without WSL

Need to install:

- https://download.virtualbox.org/virtualbox/7.0.14/VirtualBox-7.0.14-161095-Win.exe
- https://developer.hashicorp.com/vagrant/install

## Use putty with vagrant (not work on my system)

https://github.com/nickryand/vagrant-multi-putty

    vagrant plugin install vagrant-multi-putty

## Up environment

```
E:\LFS258\vm>vagrant up
Bringing machine 'cp' up with 'virtualbox' provider...
Bringing machine 'wrk1' up with 'virtualbox' provider...
Bringing machine 'wrk2' up with 'virtualbox' provider...
==> cp: Box 'ubuntu/focal64' could not be found. Attempting to find and install...
    cp: Box Provider: virtualbox
    cp: Box Version: >= 0
==> cp: Loading metadata for box 'ubuntu/focal64'
    cp: URL: https://vagrantcloud.com/api/v2/vagrant/ubuntu/focal64
==> cp: Adding box 'ubuntu/focal64' (v20240207.0.0) for provider: virtualbox
    cp: Downloading: https://vagrantcloud.com/ubuntu/boxes/focal64/versions/20240207.0.0/providers/virtualbox/unknown/vagrant.box
Download redirected to host: cloud-images.ubuntu.com
    cp:
==> cp: Successfully added box 'ubuntu/focal64' (v20240207.0.0) for 'virtualbox'!
==> cp: Importing base box 'ubuntu/focal64'...
==> cp: Matching MAC address for NAT networking...
==> cp: Checking if box 'ubuntu/focal64' version '20240207.0.0' is up to date...
==> cp: Setting the name of the VM: vm_cp_1707413167647_81766
==> cp: Clearing any previously set network interfaces...
==> cp: Preparing network interfaces based on configuration...
    cp: Adapter 1: nat
    cp: Adapter 2: hostonly
==> cp: Forwarding ports...
    cp: 6443 (guest) => 6443 (host) (adapter 1)
    cp: 22 (guest) => 2222 (host) (adapter 1)
==> cp: Running 'pre-boot' VM customizations...
==> cp: Booting VM...
==> cp: Waiting for machine to boot. This may take a few minutes...
    cp: SSH address: 127.0.0.1:2222
    cp: SSH username: vagrant
    cp: SSH auth method: private key
    cp: Warning: Connection reset. Retrying...
    cp: Warning: Connection aborted. Retrying...
    cp:
    cp: Vagrant insecure key detected. Vagrant will automatically replace
    cp: this with a newly generated keypair for better security.
    cp:
    cp: Inserting generated public key within guest...
    cp: Removing insecure key from the guest if it's present...
    cp: Key inserted! Disconnecting and reconnecting using new SSH key...
==> cp: Machine booted and ready!
==> cp: Checking for guest additions in VM...
    cp: The guest additions on this VM do not match the installed version of
    cp: VirtualBox! In most cases this is fine, but in rare cases it can
    cp: prevent things such as shared folders from working properly. If you see
    cp: shared folder errors, please make sure the guest additions within the
    cp: virtual machine match the version of VirtualBox you have installed on
    cp: your host and reload your VM.
    cp:
    cp: Guest Additions Version: 6.1.48
    cp: VirtualBox Version: 7.0
==> cp: Setting hostname...
==> cp: Configuring and enabling network interfaces...
==> cp: Running provisioner: shell...
    cp: Running: inline script
    cp: this is my kubernetes controlplane
==> wrk1: Box 'ubuntu/focal64' could not be found. Attempting to find and install...
    wrk1: Box Provider: virtualbox
    wrk1: Box Version: >= 0
==> wrk1: Loading metadata for box 'ubuntu/focal64'
    wrk1: URL: https://vagrantcloud.com/api/v2/vagrant/ubuntu/focal64
==> wrk1: Adding box 'ubuntu/focal64' (v20240207.0.0) for provider: virtualbox
==> wrk1: Importing base box 'ubuntu/focal64'...
==> wrk1: Matching MAC address for NAT networking...
==> wrk1: Checking if box 'ubuntu/focal64' version '20240207.0.0' is up to date...
==> wrk1: Setting the name of the VM: vm_wrk1_1707413265516_27961
==> wrk1: Fixed port collision for 22 => 2222. Now on port 2200.
==> wrk1: Clearing any previously set network interfaces...
==> wrk1: Preparing network interfaces based on configuration...
    wrk1: Adapter 1: nat
    wrk1: Adapter 2: hostonly
==> wrk1: Forwarding ports...
    wrk1: 22 (guest) => 2200 (host) (adapter 1)
==> wrk1: Running 'pre-boot' VM customizations...
==> wrk1: Booting VM...
==> wrk1: Waiting for machine to boot. This may take a few minutes...
    wrk1: SSH address: 127.0.0.1:2200
    wrk1: SSH username: vagrant
    wrk1: SSH auth method: private key
    wrk1: Warning: Connection reset. Retrying...
    wrk1: Warning: Connection aborted. Retrying...
    wrk1:
    wrk1: Vagrant insecure key detected. Vagrant will automatically replace
    wrk1: this with a newly generated keypair for better security.
    wrk1:
    wrk1: Inserting generated public key within guest...
    wrk1: Removing insecure key from the guest if it's present...
    wrk1: Key inserted! Disconnecting and reconnecting using new SSH key...
==> wrk1: Machine booted and ready!
==> wrk1: Checking for guest additions in VM...
    wrk1: The guest additions on this VM do not match the installed version of
    wrk1: VirtualBox! In most cases this is fine, but in rare cases it can
    wrk1: prevent things such as shared folders from working properly. If you see
    wrk1: shared folder errors, please make sure the guest additions within the
    wrk1: virtual machine match the version of VirtualBox you have installed on
    wrk1: your host and reload your VM.
    wrk1:
    wrk1: Guest Additions Version: 6.1.48
    wrk1: VirtualBox Version: 7.0
==> wrk1: Setting hostname...
==> wrk1: Configuring and enabling network interfaces...
==> wrk1: Running provisioner: shell...
    wrk1: Running: inline script
    wrk1: this is my kubernetes worker (number 1)
==> wrk2: Box 'ubuntu/focal64' could not be found. Attempting to find and install...
    wrk2: Box Provider: virtualbox
    wrk2: Box Version: >= 0
==> wrk2: Loading metadata for box 'ubuntu/focal64'
    wrk2: URL: https://vagrantcloud.com/api/v2/vagrant/ubuntu/focal64
==> wrk2: Adding box 'ubuntu/focal64' (v20240207.0.0) for provider: virtualbox
==> wrk2: Importing base box 'ubuntu/focal64'...
==> wrk2: Matching MAC address for NAT networking...
==> wrk2: Checking if box 'ubuntu/focal64' version '20240207.0.0' is up to date...
==> wrk2: Setting the name of the VM: vm_wrk2_1707413338874_91454
==> wrk2: Fixed port collision for 22 => 2222. Now on port 2201.
==> wrk2: Clearing any previously set network interfaces...
==> wrk2: Preparing network interfaces based on configuration...
    wrk2: Adapter 1: nat
    wrk2: Adapter 2: hostonly
==> wrk2: Forwarding ports...
    wrk2: 22 (guest) => 2201 (host) (adapter 1)
==> wrk2: Running 'pre-boot' VM customizations...
==> wrk2: Booting VM...
==> wrk2: Waiting for machine to boot. This may take a few minutes...
    wrk2: SSH address: 127.0.0.1:2201
    wrk2: SSH username: vagrant
    wrk2: SSH auth method: private key
    wrk2:
    wrk2: Vagrant insecure key detected. Vagrant will automatically replace
    wrk2: this with a newly generated keypair for better security.
    wrk2:
    wrk2: Inserting generated public key within guest...
    wrk2: Removing insecure key from the guest if it's present...
    wrk2: Key inserted! Disconnecting and reconnecting using new SSH key...
==> wrk2: Machine booted and ready!
==> wrk2: Checking for guest additions in VM...
    wrk2: The guest additions on this VM do not match the installed version of
    wrk2: VirtualBox! In most cases this is fine, but in rare cases it can
    wrk2: prevent things such as shared folders from working properly. If you see
    wrk2: shared folder errors, please make sure the guest additions within the
    wrk2: virtual machine match the version of VirtualBox you have installed on
    wrk2: your host and reload your VM.
    wrk2:
    wrk2: Guest Additions Version: 6.1.48
    wrk2: VirtualBox Version: 7.0
==> wrk2: Setting hostname...
==> wrk2: Configuring and enabling network interfaces...
==> wrk2: Running provisioner: shell...
    wrk2: Running: inline script
    wrk2: this is my kubernetes worker (number 2)

==> cp: Machine 'cp' has a post `vagrant up` message. This is a message
==> cp: from the creator of the Vagrantfile, and not from Vagrant itself:
==> cp:
==> cp: cp is up

==> wrk1: Machine 'wrk1' has a post `vagrant up` message. This is a message
==> wrk1: from the creator of the Vagrantfile, and not from Vagrant itself:
==> wrk1:
==> wrk1: wrk1 is up

==> wrk2: Machine 'wrk2' has a post `vagrant up` message. This is a message
==> wrk2: from the creator of the Vagrantfile, and not from Vagrant itself:
==> wrk2:
==> wrk2: wrk2 is up
```

## Configure control panel

```
        sudo apt-get update && sudo apt-get dist-upgrade -y

        # "Network interface should be consistent for the same box", he says with hope.
        private_network_ip=$(ip addr show enp0s8 | grep 'inet ' | awk '{print $2}' | awk -F\/ '{print $1}')
        echo KUBELET_EXTRA_ARGS="--node-ip $private_network_ip" | sudo tee /etc/default/kubelet


        sudo apt install curl apt-transport-https vim git wget gnupg2 \ 
        software-properties-common apt-transport-https ca-certificates uidmap -y

        sudo swapoff -a


        sudo ufw disable
        sudo ufw status

        sudo systemctl stop apparmor
        sudo systemctl disable apparmor

        sudo modprobe overlay
        sudo modprobe br_netfilter

        cat << EOF | sudo tee /etc/sysctl.d/kubernetes.conf
        net.bridge.bridge-nf-call-ip6tables = 1
        net.bridge.bridge-nf-call-iptables = 1
        net.ipv4.ip_forward = 1
        EOF

        sudo sysctl --system

        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo apt install containerd -y

        echo "deb  http://apt.kubernetes.io/  kubernetes-xenial  main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
        curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
        sudo apt-get update

        sudo apt-get install -y kubeadm=1.27.1-00 kubelet=1.27.1-00 kubectl=1.27.1-00
        sudo apt-mark hold kubelet kubeadm kubectl

        echo 10.0.0.175 k8scp k8scp.example.com | sudo tee -a /etc/hosts
        echo 10.0.0.176 wrk1 wrk1.example.com | sudo tee -a /etc/hosts
        echo 10.0.0.177 wrk2 wrk2.example.com | sudo tee -a /etc/hosts
```




https://kubernetes.io/docs/reference/config-api/kubeadm-config.v1beta3/

Create configuration kubeadm

```
cat << EOF | sudo tee kubeadm-config.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: 1.27.1
controlPlaneEndpoint: "k8scp:6443"
networking:
  podSubnet: 192.168.0.0/16
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: $(ip addr show enp0s8 | grep 'inet ' | awk '{print $2}' | awk -F\/ '{print $1}')
  bindPort: 6443
EOF

```

Helm installation:
https://helm.sh/docs/intro/install/

```
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```

Install cilium:

```
vagrant@cp:~$ find $HOME -name cilium-cni.yaml
/home/vagrant/LFS258/SOLUTIONS/s_03/cilium-cni.yaml
vagrant@cp:~$ kubectl apply -f /home/vagrant/LFS258/SOLUTIONS/s_03/cilium-cni.yaml
serviceaccount/cilium created
serviceaccount/cilium-operator created
secret/cilium-ca created
secret/hubble-server-certs created
configmap/cilium-config created
clusterrole.rbac.authorization.k8s.io/cilium created
clusterrole.rbac.authorization.k8s.io/cilium-operator created
clusterrolebinding.rbac.authorization.k8s.io/cilium created
clusterrolebinding.rbac.authorization.k8s.io/cilium-operator created
role.rbac.authorization.k8s.io/cilium-config-agent created
rolebinding.rbac.authorization.k8s.io/cilium-config-agent created
service/hubble-peer created
daemonset.apps/cilium created
deployment.apps/cilium-operator created
vagrant@cp:~$

```

Auto completion:
```
vagrant@cp:~$ sudo apt-get install bash-completion -y
vagrant@cp:~$ source <(kubectl completion bash)
vagrant@cp:~$ echo "source <(kubectl completion bash)" >> $HOME/.bashrc

```
Description of nodes:
```
vagrant@cp:~$ kubectl describe nodes cp
Name:               cp
Roles:              control-plane
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=cp
                    kubernetes.io/os=linux
                    node-role.kubernetes.io/control-plane=
                    node.kubernetes.io/exclude-from-external-load-balancers=
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: unix:///var/run/containerd/containerd.sock
                    node.alpha.kubernetes.io/ttl: 0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Thu, 08 Feb 2024 20:03:45 +0000
Taints:             node-role.kubernetes.io/control-plane:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  cp
  AcquireTime:     <unset>
  RenewTime:       Thu, 08 Feb 2024 20:32:35 +0000
Conditions:
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                 ------  -----------------                 ------------------                ------                       -------
  NetworkUnavailable   False   Thu, 08 Feb 2024 20:30:48 +0000   Thu, 08 Feb 2024 20:30:48 +0000   CiliumIsUp                   Cilium is running on this node
  MemoryPressure       False   Thu, 08 Feb 2024 20:30:52 +0000   Thu, 08 Feb 2024 20:03:42 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure         False   Thu, 08 Feb 2024 20:30:52 +0000   Thu, 08 Feb 2024 20:03:42 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure          False   Thu, 08 Feb 2024 20:30:52 +0000   Thu, 08 Feb 2024 20:03:42 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                True    Thu, 08 Feb 2024 20:30:52 +0000   Thu, 08 Feb 2024 20:30:42 +0000   KubeletReady                 kubelet is posting ready status. AppArmor enabled
Addresses:
  InternalIP:  10.0.0.175
  Hostname:    cp
Capacity:
  cpu:                2
  ephemeral-storage:  40581564Ki
  hugepages-2Mi:      0
  memory:             2018680Ki
  pods:               110
Allocatable:
  cpu:                2
  ephemeral-storage:  37399969321
  hugepages-2Mi:      0
  memory:             1916280Ki
  pods:               110
System Info:
  Machine ID:                 9f43e7cd2ebd429fa3bc5e438fe1f7c9
  System UUID:                f3f2d417-3cbe-0d4a-9d88-d9da77bda854
  Boot ID:                    ddb738cb-bdbe-48b7-a7a9-fc19cb0252f4
  Kernel Version:             5.4.0-171-generic
  OS Image:                   Ubuntu 20.04.6 LTS
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  containerd://1.7.2
  Kubelet Version:            v1.27.1
  Kube-Proxy Version:         v1.27.1
PodCIDR:                      192.168.0.0/24
PodCIDRs:                     192.168.0.0/24
Non-terminated Pods:          (9 in total)
  Namespace                   Name                                CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                                ------------  ----------  ---------------  -------------  ---
  kube-system                 cilium-kks4s                        100m (5%)     0 (0%)      100Mi (5%)       0 (0%)         2m36s
  kube-system                 cilium-operator-788c7d7585-cvbwq    0 (0%)        0 (0%)      0 (0%)           0 (0%)         2m36s
  kube-system                 coredns-5d78c9869d-9c5dx            100m (5%)     0 (0%)      70Mi (3%)        170Mi (9%)     28m
  kube-system                 coredns-5d78c9869d-9wg9l            100m (5%)     0 (0%)      70Mi (3%)        170Mi (9%)     28m
  kube-system                 etcd-cp                             100m (5%)     0 (0%)      100Mi (5%)       0 (0%)         28m
  kube-system                 kube-apiserver-cp                   250m (12%)    0 (0%)      0 (0%)           0 (0%)         28m
  kube-system                 kube-controller-manager-cp          200m (10%)    0 (0%)      0 (0%)           0 (0%)         28m
  kube-system                 kube-proxy-wg5rm                    0 (0%)        0 (0%)      0 (0%)           0 (0%)         28m
  kube-system                 kube-scheduler-cp                   100m (5%)     0 (0%)      0 (0%)           0 (0%)         28m
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests     Limits
  --------           --------     ------
  cpu                950m (47%)   0 (0%)
  memory             340Mi (18%)  340Mi (18%)
  ephemeral-storage  0 (0%)       0 (0%)
  hugepages-2Mi      0 (0%)       0 (0%)
Events:
  Type     Reason                   Age                From             Message
  ----     ------                   ----               ----             -------
  Normal   Starting                 28m                kube-proxy
  Normal   NodeAllocatableEnforced  29m                kubelet          Updated Node Allocatable limit across pods
  Normal   Starting                 29m                kubelet          Starting kubelet.
  Warning  InvalidDiskCapacity      29m                kubelet          invalid capacity 0 on image filesystem
  Normal   NodeHasSufficientMemory  29m (x8 over 29m)  kubelet          Node cp status is now: NodeHasSufficientMemory
  Normal   NodeHasSufficientPID     29m (x7 over 29m)  kubelet          Node cp status is now: NodeHasSufficientPID
  Normal   NodeHasNoDiskPressure    29m (x7 over 29m)  kubelet          Node cp status is now: NodeHasNoDiskPressure
  Normal   Starting                 28m                kubelet          Starting kubelet.
  Warning  InvalidDiskCapacity      28m                kubelet          invalid capacity 0 on image filesystem
  Normal   NodeAllocatableEnforced  28m                kubelet          Updated Node Allocatable limit across pods
  Normal   NodeHasSufficientMemory  28m                kubelet          Node cp status is now: NodeHasSufficientMemory
  Normal   NodeHasNoDiskPressure    28m                kubelet          Node cp status is now: NodeHasNoDiskPressure
  Normal   NodeHasSufficientPID     28m                kubelet          Node cp status is now: NodeHasSufficientPID
  Normal   RegisteredNode           28m                node-controller  Node cp event: Registered Node cp in Controller
  Normal   NodeReady                115s               kubelet          Node cp status is now: NodeReady

```

kubectl  -n kube-system get pod

```
vagrant@cp:~$ kubectl  -n kube-system get pod
NAME                               READY   STATUS    RESTARTS   AGE
cilium-kks4s                       1/1     Running   0          4m24s
cilium-operator-788c7d7585-cvbwq   1/1     Running   0          4m24s
cilium-operator-788c7d7585-pr476   0/1     Pending   0          4m24s
coredns-5d78c9869d-9c5dx           1/1     Running   0          30m
coredns-5d78c9869d-9wg9l           1/1     Running   0          30m
etcd-cp                            1/1     Running   0          30m
kube-apiserver-cp                  1/1     Running   0          30m
kube-controller-manager-cp         1/1     Running   0          30m
kube-proxy-wg5rm                   1/1     Running   0          30m
kube-scheduler-cp                  1/1     Running   0          30m

```

View other values we could have included in the kubeadm-config.yaml

```
vagrant@cp:~$ sudo kubeadm config print init-defaults
apiVersion: kubeadm.k8s.io/v1beta3
bootstrapTokens:
- groups:
  - system:bootstrappers:kubeadm:default-node-token
  token: abcdef.0123456789abcdef
  ttl: 24h0m0s
  usages:
  - signing
  - authentication
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: 1.2.3.4
  bindPort: 6443
nodeRegistration:
  criSocket: unix:///var/run/containerd/containerd.sock
  imagePullPolicy: IfNotPresent
  name: node
  taints: null
---
apiServer:
  timeoutForControlPlane: 4m0s
apiVersion: kubeadm.k8s.io/v1beta3
certificatesDir: /etc/kubernetes/pki
clusterName: kubernetes
controllerManager: {}
dns: {}
etcd:
  local:
    dataDir: /var/lib/etcd
i
```

## Configure worker node 1

```
vagrant@wrk1:~$ sudo -i
root@wrk1:~# apt-get update && apt-get upgrade -y
root@wrk1:~# apt install curl apt-transport-https vim git wget software-properties-common lsb-release ca-certificates -y
root@wrk1:~# swapoff -a
root@wrk1:~# modprobe overlay
root@wrk1:~# modprobe br_netfilter
root@wrk1:~# cat << EOF | tee /etc/sysctl.d/kubernetes.conf
> net.bridge.bridge-nf-call-ip6tables = 1
> net.bridge.bridge-nf-call-iptables = 1
> net.ipv4.ip_forward = 1
> EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1

root@wrk1:~# mkdir -p /etc/apt/keyrings
root@wrk1:~# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
root@wrk1:~# echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
root@wrk1:~# apt-get remove containerd.io -y
root@wrk1:~# rm /etc/containerd/config.toml
# solve error https://k21academy.com/docker-kubernetes/container-runtime-is-not-running/


echo "deb  http://apt.kubernetes.io/  kubernetes-xenial  main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update


curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt install containerd -y


private_network_ip=$(ip addr show enp0s8 | grep 'inet ' | awk '{print $2}' | awk -F\/ '{print $1}')
echo KUBELET_EXTRA_ARGS="--node-ip $private_network_ip" | sudo tee /etc/default/kubelet

sudo apt install curl apt-transport-https vim git wget gnupg2 \
software-properties-common apt-transport-https ca-certificates uidmap -y


```

```
# --------------------
# Run on Control Plane
# --------------------

# output of below looks like: 854upa.bjbcbgq0fvsxzz18
kubeadm token create

# output of below looks like: efe13a93950380e5f0da84af6369643e2d8be22b75746d7d5c922e22020022e9
openssl x509 -pubkey \
-in /etc/kubernetes/pki/ca.crt | openssl rsa \
-pubin -outform der 2>/dev/null | openssl dgst \
-sha256 -hex | awk '{print $NF}'

```

```
# ------------------
# Run on Worker Node
# ------------------
TOKEN="d3k8o9.ov19ub2wh83vgl6i"
DIGEST="b1e7eb98d2c93c1fe73016d6e4e22e29d961171dc21adedfac109c0cf54266e9"

sudo kubeadm join \
  --token "${TOKEN}" \
  k8scp:6443 \
  --discovery-token-ca-cert-hash "sha256:${DIGEST}"


```



## Error Permission denied (publickey)

Error:

        E:\LFS258\vm>vagrant ssh cp
        vagrant@127.0.0.1: Permission denied (publickey).

### Solution 1 
If this is set, Vagrant will prefer using utility executables (like ssh and rsync) from the local system instead of those vendored within the Vagrant installation.

Vagrant will default to using a system provided ssh on Windows. This environment variable can also be used to disable that behavior to force Vagrant to use the embedded ssh executable by setting it to 0.

        set VAGRANT_PREFER_SYSTEM_BIN=0  
        vagrant ssh

### Solution 2

Add in Vagrantfile **config.ssh.insert_key = false**

https://www.devopsroles.com/vagrant-ssh-permission-denied-fixed/#How_do_fix_vagrant_ssh_Permission_denied

## Connect under Windows Lens

Add to c:\Windows\System32\Drivers\etc\hosts 
        
        127.0.0.1 k8scp

## Conver key for putty

Use PuttyGen convert key from vagrant vm, location can chek by **vagrant ssh-config**

```
E:\LFS258\vm>vagrant ssh-config
Host cp
  HostName 127.0.0.1
  User vagrant
  Port 2222
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile E:/LFS258/vm/.vagrant/machines/cp/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
  PubkeyAcceptedKeyTypes +ssh-rsa
  HostKeyAlgorithms +ssh-rsa

Host wrk1
  HostName 127.0.0.1
  User vagrant
  Port 2200
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile E:/LFS258/vm/.vagrant/machines/wrk1/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
  PubkeyAcceptedKeyTypes +ssh-rsa
  HostKeyAlgorithms +ssh-rsa

Host wrk2
  HostName 127.0.0.1
  User vagrant
  Port 2201
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile E:/LFS258/vm/.vagrant/machines/wrk2/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
  PubkeyAcceptedKeyTypes +ssh-rsa
  HostKeyAlgorithms +ssh-rsa

```


## Overview
This repository serves as a reference to setting up a Kubernetes cluster on
your laptop suitable for using to complete the labs outlined in **Kubernetes
Fundamentals** (LFS258) by the Linux Foundation.

This is provided as a **Vagrantfile** and set of documentation that should
be easily reusable by anyone with a relatively recent Windows laptop.

I was frustrated when running through the labs that the only supported
configuration was to spin up cloud infrastructure, which would incur cost upon
the student outside of the existing course and examination fees.

I knew it should be possible to run locally, especially given the low-traffic 
and exploratory nature of the interactions it would be serving.

I'm a cheapskate, and wanted to play about with Kubernetes for the purposes of
certification without spending any of my own money. Don't worry, I'll still
get a round in.

## Getting Started
The happy-path is using a Windows laptop with Ubuntu running under WSL to run
Vagrant and Virtualbox. All commands are issued from within a WSL shell.

### System Requirements
I've assumed you have at least 8GiB of RAM and a hex-core processor.

We'll create 3 VMs:

  * Control Plane (2GiB, 2vCPU)
  * Worker 1 (2GiB, 2vCPU)
  * Worker 2 (2GiB, 2vCPU)

If you have less, adjust the Vagrantfile accordingly, but YMMV.

I've also assumed you don't have a very fancy network. Each VM is brought up
with one private interface (which all cluster comms happen over) and one NAT
interface to allow things like `apt-get` to work.

If you have different requirements, adjust the Vagrantfile accordingly. YMMV.

### Pre-requisites
These are things you need to have installed and configured before you can use
the things included in this repository to stand up a Kubernetes cluster.

1. Enable [WSL](https://learn.microsoft.com/en-us/windows/wsl/install) and install Ubuntu (use WSL1, see below).
2. Install [VirtualBox](https://download.virtualbox.org) for Windows.
3. Install [Vagrant](https://www.vagrantup.com/) via WSL (`apt-get install vagrant`)
4. Configure [Vagrant to use WSL](https://www.vagrantup.com/docs/other/wsl)
5. Configure your `~/.bashrc` or `~/.zshrc` with `VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"` to allow WSL Vagrant to work with Windows VirtualBox
6. Configure your `PATH` in `~/.bashrc` or `~/.zshrc` to include the Virtualbox binaries directory (e.g. `PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"`) for easy access to `VBox*.exe` binaries
7. Make sure you're not connected to a work VPN or similar.

You should be good to go now!

#### Note About WSL2
I have seen performance issues starting VirtualBox VMs under Vagrant on WSL2, such that it is unusable.

WSL1 seemed to be much more performant. VirtualBox VMs under WSL2 always used software-virtualisation and as such were dog slow.

The [Internet says](https://www.reddit.com/r/vagrant/comments/sndv7q/vagrant_wsl2_hyperv) that this is because of HyperV's involvement in WSL2 getting in the way of VirtualBox's virtualisation, which makes sense.

I ended up completely uninstalling WSL2 and reinstalling WSL1, then everything was _much better_.

At any rate, getting the tools working is outside of the scope of this repo, but I wanted to let you know of my experiences.

I found [this answer](https://stackoverflow.com/a/69896662) on StackOverflow to be very helpful at explaining everything.

### Launching the VMs
From this directory, simply run `vagrant up`.

This will create 3 completely bare Ubuntu 20.04 servers that you can access
with `vagrant ssh <cp|wrk1|wrk2>`, etc.

It'll take a little while (10/15 minutes or so) as launching each box patches
some operating system packages and salso installs the VBoxGuestAdditions.

Now you can move onto the steps for configuring the control planes and workers.

I've kept these relatively verbose. It's useful to know what the commands do.

Follow along with LFS258 Lab 3 if you like.

## Configuring the Control Plane
Log into the Control Plane virtual machine.

```
vagrant ssh cp
```

Install any outstanding patches or OS updates.

```
sudo apt-get update && sudo apt-get dist-upgrade -y
```

Before we install Kuberentes, let's get ahead of things by setting the Node IP
of this system to the private IP address. This will help us force Kubernetes
traffic over the private network rather than confusing things by using the NAT
interface.

```
# "Network interface should be consistent for the same box", he says with hope.
private_network_ip=$(ip addr show enp0s8 | grep 'inet ' | awk '{print $2}' | awk -F\/ '{print $1}')
echo KUBELET_EXTRA_ARGS="--node-ip $private_network_ip" | sudo tee /etc/default/kubelet
```

Install some software dependencies

```
sudo apt install curl apt-transport-https vim git wget gnupg2 \
software-properties-common apt-transport-https ca-certificates uidmap -y
```

Turn off swap.

```
sudo swapoff -a
```

Turn off the firewall and AppArmor. It's cool, we're learning.

```
sudo ufw disable
sudo ufw status

sudo systemctl stop apparmor
sudo systemctl disable apparmor
```

Ensure we've loaded certain kernel modules needed by Kubernetes.

```
sudo modprobe overlay
sudo modprobe br_netfilter
```

Configure and apply some kernel parameters needed by Kubernetes.

```
cat << EOF | sudo tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sudo sysctl --system
```

Install ContainerD

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt install containerd -y
```

Install and pin the version of Kubernetes that the labs want (12.23.1).

```
echo "deb  http://apt.kubernetes.io/  kubernetes-xenial  main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update

sudo apt-get install -y kubeadm=1.23.1-00 kubelet=1.23.1-00 kubectl=1.23.1-00
sudo apt-mark hold kubelet kubeadm kubectl
```

Get the manifest for Calico.

```
wget https://docs.projectcalico.org/manifests/calico.yaml
```

Ensure that the setting `CALICO_IPV4POOL_CIDR` does not clash with IP ranges we're using.

```
grep -A1 CALICO_IPV4POOL_CIDR calico.yaml
```

By default this is set to 192.168.0.0/16, which is fine. 

This is the range containers on our cluster will get.

Now, for convenience configure some `/etc/hosts` entries for our other cluster nodes.

```
echo 10.0.0.175 k8scp k8scp.example.com | sudo tee -a /etc/hosts
echo 10.0.0.176 wrk1 wrk1.example.com | sudo tee -a /etc/hosts
echo 10.0.0.177 wrk2 wrk2.example.com | sudo tee -a /etc/hosts
```

Now create a Kubernetes cluster configuration. Custom as we've got multiple IPs

```
cat << EOF | sudo tee kubeadm-config.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: 1.23.1
controlPlaneEndpoint: "k8scp:6443"
networking:
  podSubnet: $(grep -A1 CALICO_IPV4POOL_CIDR calico.yaml | grep value | awk '{print $NF}' | tr -d \")
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: $(ip addr show enp0s8 | grep 'inet ' | awk '{print $2}' | awk -F\/ '{print $1}')
  bindPort: 6443
EOF
```

Initialise the control plane.

```
sudo kubeadm init --config=kubeadm-config.yaml \
    --upload-certs \
    | sudo tee kubeadm-init.out
```

Configure `kubectl`.

```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Apply the Calico network config to the cluster:

```
kubectl apply -f calico.yaml
```

Configure `kubectl` bash completion

```
source <(kubectl completion bash)
echo "source <(kubectl completion bash)" >> $HOME/.bashrc
```

Running `kubectl` commands should now work in general, and we're done.

```
kubectl get nodes
```

## Configuring Worker Nodes
You can either do these one at a time or both at once for expedience.

Some of this will look familiar. Some of it will need you to get stuff from
the Kubernetes control plane node we just set up, which I'll **highlight**.

First, log into the worker node.

```
vagrant ssh wrk1
```

Install any outstanding patches or OS updates.

```
sudo apt-get update && sudo apt-get dist-upgrade -y
```

Before we install Kuberentes, let's get ahead of things by setting the Node IP
of this system to the private IP address. This will help us force Kubernetes
traffic over the private network rather than confusing things by using the NAT
interface.

```
# "Network interface should be consistent for the same box", he says with hope.
private_network_ip=$(ip addr show enp0s8 | grep 'inet ' | awk '{print $2}' | awk -F\/ '{print $1}')
echo KUBELET_EXTRA_ARGS="--node-ip $private_network_ip" | sudo tee /etc/default/kubelet
```

Install some software dependencies

```
sudo apt install curl apt-transport-https vim git wget gnupg2 \
software-properties-common apt-transport-https ca-certificates uidmap -y
```

Turn off swap.

```
sudo swapoff -a
```

Turn off the firewall and AppArmor. It's cool, we're learning.

```
sudo ufw disable
sudo ufw status

sudo systemctl stop apparmor 
sudo systemctl disable apparmor
```

Ensure we've loaded certain kernel modules needed by Kubernetes.

```
sudo modprobe overlay
sudo modprobe br_netfilter
```

Configure and apply some kernel parameters needed by Kubernetes.

```
cat << EOF | sudo tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sudo sysctl --system
```

Install ContainerD

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt install containerd -y
```

Install and pin the version of Kubernetes that the labs want (12.23.1).

```
echo "deb  http://apt.kubernetes.io/  kubernetes-xenial  main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update

sudo apt-get install -y kubeadm=1.23.1-00 kubelet=1.23.1-00 kubectl=1.23.1-00
sudo apt-mark hold kubelet kubeadm kubectl
```

For convenience, configure some DNS using `/etc/hosts`:

```
echo 10.0.0.175 k8scp k8scp.example.com | sudo tee -a /etc/hosts
echo 10.0.0.176 wrk1 wrk1.example.com | sudo tee -a /etc/hosts
echo 10.0.0.177 wrk2 wrk2.example.com | sudo tee -a /etc/hosts
```

Now, **on the Control Plane node, get a 2hr duration token we can use to join the cluster**

```
# --------------------
# Run on Control Plane
# --------------------

# output of below looks like: 854upa.bjbcbgq0fvsxzz18
kubeadm token create

# output of below looks like: efe13a93950380e5f0da84af6369643e2d8be22b75746d7d5c922e22020022e9
openssl x509 -pubkey \
-in /etc/kubernetes/pki/ca.crt | openssl rsa \
-pubin -outform der 2>/dev/null | openssl dgst \
-sha256 -hex | awk '{print $NF}'
```

Take note of the output you get. You'll need it shortly.

Now, **on the Worker Node** you can join the cluster:

```
# ------------------
# Run on Worker Node
# ------------------
TOKEN="854upa.bjbcbgq0fvsxzz18"
DIGEST="efe13a93950380e5f0da84af6369643e2d8be22b75746d7d5c922e22020022e9"

sudo kubeadm join \
  --token "${TOKEN}" \
  k8scp:6443 \
  --discovery-token-ca-cert-hash "sha256:${DIGEST}"
```

Now, **on the Control Plane** you should see the worker node has joined and is Ready.

It will take about 5 minues to transition from `NotReady` to `Ready`.

Now's a good time to make sure that second worker node is also configured correctly.

You should now have a fully configured Kubernetes cluster. Hooray!


## Deployments
The world is your oyster now, but here's a sample "Hello, World!" you can use
to confirm everything's working as you expect.

Run all of these **from the Control Plane** node.

```
kubectl create deployment nginx --image=nginx
```

You should see the deployment go to Running:

```
vagrant@cp:~$ kubectl get pod -o wide
NAME                     READY   STATUS              RESTARTS   AGE    IP              NODE   NOMINATED NODE   READINESS GATES
nginx-85b98978db-dmtvl   0/1     ContainerCreating   0          100s   192.168.5.193   wrk1   <none>           <none>

...wait...

vagrant@cp:~$ kubectl get pod -o wide
NAME                     READY   STATUS    RESTARTS   AGE    IP              NODE   NOMINATED NODE   READINESS GATES
nginx-85b98978db-dmtvl   1/1     Running   0          100s   192.168.5.193   wrk1   <none>           <none>
```

And then hitting that IP should show return a webpage.

```
vagrant@cp:~$ curl 192.168.5.193
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

