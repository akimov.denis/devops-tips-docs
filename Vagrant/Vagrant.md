# Vagrant

## SSH key for Vagrant connection

1. convert the %USERPROFILE%\.vagrant.\insecure_private_key\vagrant.keyrsa to .ppk using PuTTYGen save as private key
1. use the .ppk key in your PuTTY session - configured in Connection > SSH > Auth > Private key file
1. use host 127.0.0.1
1. use port 2222
1. you can set the default username (vagrant) under Connection > SSH > Auth > Private key for authentication or in Session > Host Name (vagrant@127.0.0.1)

## VSCode remote ssh config for Vagrant

```
Host vagrant
  HostName 127.0.0.1
  User vagrant
  Port 2222
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile C:\Users\User\.vagrant.d\insecure_private_keys\vagrant.key.rsa
  IdentitiesOnly yes
  LogLevel FATAL
  ForwardAgent yes
  ForwardX11 yes
```