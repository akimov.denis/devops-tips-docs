# SSL Connection in Groovy
```
import java.nio.file.Path
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.KeyManagerFactory
import java.security.KeyStore

static def HTTPS_TARGET = "https://cexample.com:8080/xxx"
static def TRUST_STORE_PATH = "/root/script_trust.keystore"
static def KEY_STORE_PATH = "/root/script.keystore"
static def KEY_STORE_PASSWORD = "changeit"
static def TRUST_STORE_PASSWORD = "changeit"

SSLContext sc = null

String keyStoreType = "JKS"
Path trustStorePath = Paths.get(TRUST_STORE_PATH)
InputStream trustStoreStream = Files.newInputStream(trustStorePath, StandardOpenOption.READ)
KeyStore trustStore = KeyStore.getInstance(keyStoreType)
trustStore.load(trustStoreStream, TRUST_STORE_PASSWORD.toCharArray())
String trustManagerFactoryAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(trustManagerFactoryAlgorithm)
trustManagerFactory.init(trustStore)
trustStoreStream.close()

Path identityPath = Paths.get(KEY_STORE_PATH)
InputStream identityStream = Files.newInputStream(identityPath, StandardOpenOption.READ)
KeyStore identity = KeyStore.getInstance(keyStoreType)
identity.load(identityStream, KEY_STORE_PASSWORD.toCharArray())
String keyManagerFactoryAlgorithm = KeyManagerFactory.getDefaultAlgorithm()
KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(keyManagerFactoryAlgorithm)
keyManagerFactory.init(identity, KEY_STORE_PASSWORD.toCharArray())
identityStream.close();

sc = SSLContext.getInstance("TLS")
sc.init(
        keyManagerFactory.getKeyManagers(),
        trustManagerFactory.getTrustManagers(),
        null
)

def message = "Test"

//Use a Map as Interface Implementation of HostnameVerifier.verify() https://blog.mrhaki.com/2009/08/groovy-goodness-use-map-as-interface.html
def hostnameVerifier = [verify: { hostname, session -> true }]
def response = new URL(HTTPS_TARGET).openConnection()
response.setSSLSocketFactory(sc.socketFactory)
response.setHostnameVerifier(hostnameVerifier as HostnameVerifier)
response.setRequestMethod("POST")
response.setDoOutput(true)
response.setRequestProperty("Content-Type", "application/json")
response.getOutputStream().write(message.getBytes("UTF-8"))

def responseRC = response.getResponseCode()
```