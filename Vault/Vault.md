# Vault notes

## Vault demonstration with Gitlab-CI and AWS

- [Vault demonstration with Gitlab-CI and AWS](https://github.com/mehdilaruelle/vault-aws-gitlab-secret/blob/master/README.md)

## VAULT_TOKET

    export VAULT_TOKEN="$(vault write -field=token auth/jwt/login role=${VAULT_ROLE} jwt=$CI_JOB_JWT)"