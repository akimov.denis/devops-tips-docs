# Devops Tips Docs


## Links to guide

- [Kubernetes Operator](KubernetesOperator/KubernetesOperator.md)
- [IaC](IaC/IaC.md)
- [Vault](Vault/Vault.md)
- [Vagrant](Vagrant/Vagrant.md)



